-- Answers to Exercise 5 here
DROP TABLE IF EXISTS ex5_user_info;

CREATE TABLE IF NOT EXISTS ex5_user_info (
  username VARCHAR(30) NOT NULL,
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  emla VARCHAR(50),
  PRIMARY KEY (username)
);

INSERT INTO ex5_user_info VALUES ('peter_wilson', 'Peter', 'Wilson', 'perter_wilson@gmail.com');
INSERT INTO ex5_user_info VALUES ('pete_cooper', 'Pete', 'Copper', 'pete_cooper@gmail.com');
INSERT INTO ex5_user_info VALUES ('mia_peterson', 'Mia', 'Peterson', 'mia_peterson@gmail.com');
INSERT INTO ex5_user_info VALUES ('bill_gates', 'Bill', 'Gates', 'bill_gates@gmail.com');

