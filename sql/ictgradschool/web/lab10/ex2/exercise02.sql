-- Answers to Exercise 2 here
DROP TABLE IF EXISTS user_info;

CREATE TABLE IF NOT EXISTS user_info (
  username VARCHAR(30) NOT NULL,
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  emla VARCHAR(50),
  PRIMARY KEY (username)
);

INSERT INTO user_info VALUES ('peter_wilson', 'Peter', 'Wilson', 'perter_wilson@gmail.com');
INSERT INTO user_info VALUES ('pete_cooper', 'Pete', 'Copper', 'pete_cooper@gmail.com');
INSERT INTO user_info VALUES ('mia_peterson', 'Mia', 'Peterson', 'mia_peterson@gmail.com');
INSERT INTO user_info VALUES ('bill_gates', 'Bill', 'Gates', 'bill_gates@gmail.com');


INSERT INTO user_info VALUES ('peter_wilson', 'Peter', 'Wilson', 'perter_wilson@gmail.com');
