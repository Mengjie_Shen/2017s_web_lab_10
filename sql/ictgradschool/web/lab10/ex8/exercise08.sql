-- Answers to Exercise 8 here
DELETE FROM ex5_user_info
WHERE username='bill_gates';

ALTER TABLE ex5_user_info DROP COLUMN emla;

DROP TABLE ex5_user_info;

UPDATE movie_store
SET on_loan = 'Michael Hurst'
WHERE bar_code = 4;

UPDATE movie_store
SET weekly_charge = 2
WHERE bar_code = 4;

UPDATE movie_store
SET bar_code = 6
WHERE bar_code = 4;


