-- Answers to Exercise 4 here
DROP TABLE IF EXISTS lorem_ipsum;

CREATE TABLE IF NOT EXISTS lorem_ipsum (
  id INT NOT NULL,
  title VARCHAR(30),
  content TEXT,
  PRIMARY KEY (id)
);


INSERT INTO lorem_ipsum VALUES (1, 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla auctor eleifend maximus. In ac rutrum nisi. Ut aliquet pulvinar risus, nec mattis nunc aliquam in. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc egestas ligula eu tellus rutrum, eget semper lacus placerat. Vestibulum ultrices enim eu efficitur laoreet. Pellentesque vehicula consequat consectetur. Etiam sodales augue non ex rutrum vehicula. Sed rutrum sodales metus non cursus. Ut tempor et dolor id rhoncus. Cras ut eros ac urna tincidunt fringilla eget quis tellus. Morbi tortor leo, egestas non ipsum at, tempus porttitor tortor.');
INSERT INTO lorem_ipsum VALUES (2, 'Ut eros massa', 'Ut eros massa, sodales vitae blandit id, congue eu lectus. Fusce ornare pulvinar sagittis. Etiam nec neque sagittis, finibus dui et, porta ex. Proin cursus malesuada orci, in tincidunt leo ultrices eget. Pellentesque vel urna vel velit pretium tempus vel ut metus. Suspendisse potenti. In quis vehicula arcu, vitae vulputate mi. Maecenas non imperdiet est, ullamcorper dictum lectus. Etiam consequat posuere metus eu pharetra. Maecenas sit amet nibh a leo commodo rhoncus. Cras non lacus at nisi tempor bibendum. Curabitur posuere est et posuere ullamcorper.');
