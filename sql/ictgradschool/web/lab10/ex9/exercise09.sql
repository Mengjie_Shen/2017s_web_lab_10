-- Answers to Exercise 9 here
SELECT * FROM video_rental_statistics;

SELECT name, gender, year_born, joined FROM video_rental_statistics;

SELECT title FROM lorem_ipsum;

SELECT DISTINCT director FROM movie_store;

SELECT title FROM movie_store
WHERE weekly_charge <= 2;

SELECT username
FROM user_info
ORDER BY username ASC;

SELECT username
FROM user_info
WHERE user_info.first_name LIKE "Pete%";

SELECT username
FROM user_info
WHERE user_info.first_name LIKE "Pete%" OR user_info.last_name LIKE "Pete%";

