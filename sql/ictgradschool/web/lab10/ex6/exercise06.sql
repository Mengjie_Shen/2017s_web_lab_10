-- Answers to Exercise 6 here
DROP TABLE IF EXISTS movie_store;

CREATE TABLE IF NOT EXISTS movie_store (
  bar_code INT NOT NULL,
  title VARCHAR(100),
  director VARCHAR(30),
  weekly_charge INT,
  on_loan VARCHAR(30),
  FOREIGN KEY (on_loan) REFERENCES video_rental_statistics (name),
  PRIMARY KEY (bar_code)
);


INSERT INTO movie_store VALUES (1, 'From Russia With Love', 'Terence Young', 2, 'Jane Campion');
INSERT INTO movie_store VALUES (2, 'The Long Voyage Home', 'John Ford', 4, 'Kate Sheppard');
INSERT INTO movie_store VALUES (3, 'Dark Shadows', 'Tim Burton', 6, 'Lucy Lawless');
INSERT INTO movie_store VALUES (4, 'Charlie and the Chocolate Factory', 'Tim Burton', 4, 'Peter Jackson');
INSERT INTO movie_store VALUES (5, 'The Iron Lady', 'Phylliday Lloyd', 2, 'Russell Crowe');