-- Answers to Exercise 7 here
DROP TABLE IF EXISTS article_comments;

CREATE TABLE IF NOT EXISTS article_comments (
  comment_id INT NOT NULL AUTO_INCREMENT,
  comment    TEXT,
  article_id INT,
  FOREIGN KEY (article_id) REFERENCES lorem_ipsum (id),
  PRIMARY KEY (comment_id)
);


INSERT INTO article_comments (comment,article_id) VALUES
  ('Good', 1),
  ('Excellent', 2),
  ('Bad', 1),
  ('Not bad', 2);